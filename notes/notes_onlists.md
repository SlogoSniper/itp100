# Lesson 9

1. In the first video of this lesson, Dr. Chuck discusses two very important
   concepts: algorithms and data structures. How does he define these two
   concepts? Which one does he say we have been focusing on until now?

   He says we have been focusing on Algorithms and algorithms are a set of rules   or steps used to solve a problem. Data Structures are a particular way of
   organizing data in a computer.


2. Describe in your own words what a collection is.

   Collections are ways to put many values in the same variable.


3. Dr. Chuck makes a very important point in the slide labeled Lists and
   definite loops - best pals about Python and variable names that he has made
   before, but which bares repeating. What is that point?
   
   He says that the variable names are arbitrary and python doesn't care what
   they are named.


4. How do we access individual items in a list?

   We can use square brackets []


5. What does mutable mean? Provide examples of mutable and immutable data types    in Python.
   
   Mutable is another word for changeable. Strings are immutable and lists are
   mutable.


6. Discribe several list operations presented in the lecture.

   You can concatenate lists with a +. You can also slice lists with [:].


7. Discribe several list methods presented in the lecture.

   count looks for ertain values in the list, extend adds things to the end of
   the list, index looks things up in the list, insert adds things to the middle   of the list, pop pulls things off the top, remove removes an item in the
   middle, reverse flips the order of the list, and sort puts them sort of into    order based on the values.

 
8. Discribe several useful functions that take lists as arguments presented in
   the lecture.

   The len() function finds the length of the function, max() finds the biggest 
   value in the list, min() finds the smallest value in the list, and sum() adds   the values in the list.


9. The third video describes several methods that allow lists and strings to
   interoperate in very useful ways. Describe these.

   You can use split on a string and it will split each word in the string into
   single items in a list. 

 
10. What is a guardian pattern? Use at least one specific example in describing     this important concept. 

    A guardian pattern is something you use as a check so that the program
    doesn't blow up. 

    if line == '':
       print('Skip Blank')
       continue
    This skips blank lines so that the next line which is 
    if wds[0] != 'From':
       continue
    
