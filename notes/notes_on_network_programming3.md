1. What is ASCII? What are its limitations and why do we need to move beyond it?
   
   ASCII is American Standard Code for Information Interchange. The limitations are 
   that you can't do any other languages. It also isn't acceptable for todays
   complexity.
   
    
2. Which function in Python will give you the numeric value of a character (and thus
   its order in the list of characters)?
   
   The function is ord().
   
   
3. Dr. Chuck says we move from a simple character set to a super complex character set.   What is this super complex character set called?
   
   It is called Unicode. The best type for most things is UTF-8 which is 1-4 bytes.
   
   
4. Describe bytes in Python 3 as presented in the video. Do a little web searching to
   find out more about Python bytes. Share something interesting that you find.
   
   In Python3 all strings are Unicode instead of a byte like it was in Python2. Byte 
   variables are an array of bytes where 'abc' is just 3 bytes but when 'abc' is a 
   string it can have 3 to 12 bytes. I found that there is a bytes() function that
   takes 3 parameters. It takes a source, encoding, and erros all of which are 
   optional.
   
   
5. Break down the process of using .encode() and .decode methods on data we send over a   network connection.
   
   When you use .encode() it takes a unicode string and turns it into UTF-8 bytes. And    when you use .decode() it takes the UTF-8 bytes and turns it back into a unicode
   string.
