# Lesson 6

1. Dr. Chuck calls looping the 4th basic pattern of computer programming. What are the other three patterns?
   
   The other three are: Sequential, Conditional, and Store and Reuse.


2. Describe the program Chr. Chuck uses to introduce the while loop. What does it do?

   He introduces us to some program in which the while loop checks to see if n is greater than 0 and if it is
   then it prints(n) and then it makes n get n - 1. 


3. What is another word for looping introduced in the video?
   
   Repeated Steps

   
4. Dr. Chuck shows us a broken example of a loop. What is this kind of loop called? What is wrong with it?
 
   This is an infinite loop which doesn't have an iteration variable.

 
5. What Python statement (a new keyword for us, by the way) can be used to exit a loop?
   
   You can use the 'break' keyword.


6. What is the other loop control statement introduced in the first video? What does it do?

   Another satement is 'continue' and it goes back to the top of loop.


7. Dr. Chuck ends the first video by saying that a while is what type of loop? Why are they called that? What is
   the type of loop he says will be discussed next?

   He says that while loops are called Indefinite Loops because they keep going until a logical condition becomes
   False. The type of loop in the next video will be Definite Loops.


8. Which new Python loop is introduced in the second video? How does it work?

   The for loop. It only runs a certian number of times depending on what we are running it for.


9. The third video introduces what Dr. Chuck calls loop idioms. What does he mean by that term? Which examples
   of loop idioms does he introduce in this and the fourth video?  

   Loop Idioms are patterns on how we make loops. An example is where he explains how to make code that would find    the largest value in a set of numbers. Another one is counting the amount of items in a list, finding the average in a list, and filtering in a loop.  
