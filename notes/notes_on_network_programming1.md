# Lesson 13


1. Dr. Chuck mentions the architecture that runs our network. What is the name of this   architecture?

   The architecture that runs our network is TCP (Tramsport Control Protocol)
 

2. Take a close look at the slide labelled Stack Connections. Which layer does Dr.
   Chuck say we will be looking at, assuming that one end of this layer can make a
   phone call to the corresponding layer on the other end? What are the names of the
   two lower level layers that we will be abstracting away?

   He says that we will be looking at the Transport layer and will be skipping over
   the Internet layer and Link layer.


3. We will be assuming that there is a nice reliable pipe that goes from point A to
   point B. There is a socket running at each end of the connection. Fill in the
   blanks.


4. Define Internet socket as discussed in the video.

   An internet socket is the endpoint of the communication flow across an Internet 
   Protocol-based computer network.


5. Define TCP port as discussed in the video.

   A Port number is best thought of like a phone number and it is used to idetify 
   what's what.


6. At which network layer do sockets exist?

   Sockets are at the Application level.


7. Which network protocol is used by the World Wide Web? At which network layer does
   it operate?

   WWW uses HTTP which exists at the Application level.
