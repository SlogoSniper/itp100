# Lesson 13

1. What is an application protocol? List examples of specific application protocols
   listed in the lecture. Can you think of one besides HTTP that we have been using in    our class regularly since the beginning of the year?
   
   An application protocol is the part of the internet that asks what we are going to     send and what we are going to get back. Another example would be the World Wide Web.
   
   
2. Name the three parts of a URL. What does each part specify?
   
   The three parts of a URL are the protocol, host, then the document. The protocol 
   specifies which protocol is being used, the host says who or what is hosting the 
   webpage and the document says what page or part of the webpage you are looking at.
   
   
3. What is the request/response cycle? What example does Dr. Chuck use to illustrate it   and describe how it works?
   
   When you click on a link it makes a connection to the port that the web server is on   and then sends a request to the server, the server then responds and in the example    of Dr. Chucks website, it returns a small webpage that is the second page of his 
   website.
   
   
4. What is the IETF? What is an RFC?
   
   IETF is the Internet Engineering Task Force, they develop the standards for all the    Internet protocols. RFC is a Request for Comments which is like a tag that has been    created to help improve on previous protocols.
   
   
5. In the video titled Worked Example: Sockets, Dr. Chuck tells us where to download a    large collection of sample programs he has available associated with the course. 
   Where do we find these examples?
   
   You can find them under Materials on the PY4E website.
   
   
6. Try the telnet example that Dr. Chuck shows us in the Worked Example: Sockets video.   Can you retrieve the document using telnet? (NOTE: examples in the previous videos
   no longer work. I suspect this is because HTTP/1.0 is no longer supported by the
   webserver).
   
   When I tried the code in my terminal it didn't retrive the webpage that Dr. Chuck
   had gotten in his example. I ended up getting a Bad Request with a message of "Your    browser sent a request that this server could not understand."   

