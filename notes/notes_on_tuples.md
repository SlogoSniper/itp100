# Lesson 11

1. Dr. Chuck begins this lesson by stating that tuples are really a lot like
   lists. In what ways are tuples like lists? Do you agree with his statement
   that they are a lot alike?
   
   Both lists and tuples have indexed elements that you can look up. You can
   also loop through tuples for what's in it. I agree that tuples are a lot like   lists in that they are basicly the same.


2. How do tuples essentially differ from lists? Why do you think there is a need   for this additional data type if it is similar to a list?

   Tuples differ from lists in that you cannot modify what's in the tuple after
   it is created where you can modify a list. Tuples are more efficient for
   your machine if all you want to do is look a a list to read it but never
   change it.


3. Dr. Chuck says that tuple assignment is really cool (Your instructor
   completely agrees with him, btw). Describe what tuple assignment is, showing    a few examples of it in use. Do you think it is really cool? Why or why not?

   You can use a tuple of variables and then assign them a tuple.    
   `(x, y) = 4, 'fred')`    
   `print(y)`  
   `fred`  
   I think that this is really cool that you can then assign multiple different
   variables multiple different values in just 1 line.


4. Summarize what you learned from the second video in this lesson. Provide
   example code to make support what you describe.

   You can sort the items in a dictionary by using the function sorted().   
   `d = {'a':10, 'b':1, 'c':22}`   
   `t = sorted(d.items())`   
   `for k, v in sorted(d.items()):`   
   `   print(k, v)`   
   `a 10`   
   `b 1`   
   `c 22`   
   You can also use tuples to flip the dictionary so its value, key instead of
   key, value.   
   `c = {'a':10, 'b':1, 'c':22}`    
   `tmp = list()`   
   `for k, v in c.items():`   
   `   tmp.append((v, k))`   
   `tmp = sorted(tmp, reverse=True)`   
   `print(tmp)`   
   `[(22, 'c'), (10, 'a'), (1, 'b')]`   
    

5. In the slide titled Even Shorter Version Dr. Chuck introduces list 
   comprehensions. This is another really cool feature of Python. Did the
   example make sense to you? Do you think you understand what is going on?
   
   I understood the example he showed us after he explained it. When I first
   looked at it I was a bit confused but after he explained how it worked 
   and what each part was doing I felt I was able to understand it.
