# Lesson 12

1. What are regular expressions? What are they used for? When did they originate   ?

   Regular expressions are a way to match strings of text. Originated in the
   1960s.   


2. Use Markdown to reproduce the Regular Expression Quick Guide slide.

     ^   Matches the **beginning** of a line   
     $   Matches the **end** of the line   
     .   Matches **any** character   
     \s  Matches **whitespace**   
     \S  Matches any **non-whitespace** character   
  `  *`  **Repeats** a character zero or more times   
  `  *?` **Repeats** a character zero or more times (non-greedy)   
  `  +`  **Repeats** a character one or more times   
  `  +?` **Repeats** a character one or more times (non-greedy)   
 [aeiou] Matches a single character in the listed **set**   
`[^XYZ]` Matches a single charcter **not in** the listed **set**   
[a-z0-9] The set of characters can include a **range**   
(        Indicates where string **extraction** is to start   
)        Indicates where string **extraction** is to end      
   

3. Make a text file with 10 lines in it. Write a python program that reads the
   contents of the file into a list of lines, and create a regular expression,
   rexp that will select exactly three items from lines.

   (https://gitlab.com/SlogoSniper/itp100/-/blob/master/exercises/regex_test.py)


4. Do a web search and find a few good resources for using regular expressions
   in vim.
  
   https://docs.python.org/3/library/re.html#module-re   
   https://docs.python.org/3/howto/regex.html   
   http://vimregex.com/   
   https://learnbyexample.gitbooks.io/vim-reference/content/Regular_Expressions.html
