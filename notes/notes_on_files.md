# Lesson 8

1. What function does Python use for accessing files? What arguments does it need? What does it return?

   The open() function is used to open files. It needs the file name and an optional argument is to read or write   it. It returns the file handle.
   >>> fhand = open('mbox.txt')
   >>> print(fhand)
    <_io.TextIOWrapper name='mbox.txt' mode='r' encoding='UTF-8'>


2. What is a file handle? Explain how it relates to a file? Where is the data stored?
   A file handle is like an opening outside the program which allows you to access the file. The data is stored
   in the file.


3. What is '\n'?
   
   \n is special character called the newline which creates a newline in strings.


4. What does Dr. Chuck say is the most common way to treat a file when reading it?

   The most common way is by treating a file as a sequence of lines. 
   >>> xfile = open('mbox.txt')
   >>> for cheese in xfile:     
          print(cheese)
   This code would take the file mbox.txt and in the for loop it will run through the file for as many lines 
   there are in the file and then prints it.


5. In the Searching Through a File (fixed) example, Dr. Chuck talks about the problem of the extra newline
   character that appears when we print out each line. He resolves this problem by using line.rstrip(), invoking    Python's built-in rstrip method of strings. Could we also use a slice here, and write line[:-1] instead?
   Explain your answer.
 
   Yes you could use [:-1] instead. [:-1] removes only the last character in the string so it would work in 
   removing the \n.
      
   
6. The second video presents three different ways, or patterns for selecting lines that start with 'From:'.
   Compare these three patterns, providing examples of each.
  
   One way is using a for loop and checking if the line startswith 'From'.
   >>> fhand = open('mbox-short.txt')
   >>> for line in fhand:
           line = line.rstrip()
           if line.startswith('From:'):
               print(line)
   A second way is using a for loop and checking if the line doesn't start with 'From:' then it continues and
   skips the line.
   >>> fhand = open('mbox-short.txt')
   >>> for line in fhand:
           line = line.rstrip()
           if not line.startswith('From:'):
               continue
           print(line)
   Another way is using the in operator and checking if 'From:' is not in the line then it continues and skips
   the line.
   >>> fhand = open('mbox-short.txt')
   >>> for line in fhand:
           line = line.rstrip()
           if not 'From:' in line:
               continue
           print(line)   


7. A new Python construct is introduced at the end of the second video to deal with the situation when the
   program attempts to open a file that isn't there. Describe this new construct and the two new keywords that
   pair to make it.

   He introduces the try and except keywords which will first try whats in the try and if that fails then it will   go to the except and run what's in the except.
