# Pair Programming

1. Why pair program? What benefits do the videos claim for the practice?

   The video claims that you should pair program because it helps to make better code because your partner might see a solution that you   don't see and can help the program.


2. What are the two distinct roles used in the practice of pair programming? How does having only one computer for two programmers
   aide in the pair adopting these roles?

   The two roles are the driver and the navigator and the driver is the one using the computer while the navigator is keeping track of
   the bigger picture. Only one computer makes sure that the two programs are collaborating and are both on the same page.
