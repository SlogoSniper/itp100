# Lesson 5

1. How can we use Markdown and git to create nice looking documents
   under revision control?
   
   Markdown is a simple way to add style to documents and is supported by
   almost everything. Markdown is supported by git. Also if for some reason 
   What you're using stops working then if you have markdown it will stransfer
   the styles.  


2. Dr. Chuck describes functions as store and reused pattern in programming.
   Explain what he means by this. (Note: I will present another view of
   functions, as a tool for allowing procedural abstraction, in our class
   discussion, and will emphasize just what an important idea that is).

   He means that functions store parts of code and then each time you call the
   function it reuses the code inside of it.


3. How do we create functions in Python? What new keyword do we use? Provide
   your own example of a function written in Python.
   
   We use the def keyword. An example would be
   def thing():
       print('Hello')
       print('Fun')
   thing()


4. Dr. Chuck shows that nothing is output when you define a function, what he
   calls the store phase. What does he call the process that makes the function 
   run? He uses two words for this, and it is really important to understand
   this idea and learn the words for it.

   He calls it the invoking phase. 


5. Provide some examples of built-in function in Python.
   
   print(), input(), type(), float(), and int() 
