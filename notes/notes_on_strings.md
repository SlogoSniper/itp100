# Lesson 7

1. What does Dr. Chuck say about indexing strings? How does this operation work? Provide a few examples.

   He says that in strings each character in the string has a value. The index operator is [] and when you put a 
   number into it then it selects the letter that is in that spot in the string. The word banana would have
   values 0-5 for each character.


2. Which Python function returns the number or characters in a string? Provide a few examples of it being used.
  
   You can use the len function.
   >>> fruit = 'banana'
   >>> index = 0
   >>> while index < len(fruit):
         letter = fruit[index]
         print(index, letter)
         index = index + 1
   0 b
   1 a
   2 n
   3 a
   4 n
   5 a


3. Discuss the two ways Dr. Chuck shows to loop over the characters in a string. Provide an example of each.

   One way is using a while loop comparing the value of a variable to the value of how long the string is and as
   long as the value of the variable is less than the value of the string then it will keep looping.
   >>> fruit = 'banana'
   >>> index = 0
   >>> while index < len(fruit):
         letter = fruit[index]
         print(index, letter)
         index = index + 1
   The other way is by using a for loop and using an iteration variable to run the loop as many times as there is   characters in the string.
   >>> fruit = 'banana'
   >>> for letter in fruit:
          print(letter)


4. What is slicing? Explain it in your own words and provide several examples of slicing at work. including
   examples using default values for start and end.

   Slicing is taking part of a string and returning it. 
   >>> s = 'Monty Python'
   >>> print(s[0:4])
   Mont
   >>> print(s[6:7])
   P
   >>> print(s[:])
   Monty Python


5. What is concatenation as it applies to strings. Show an example.

   Concatenation for strings is when you combined 2 strings together.
   >>> a = 'Hello'
   >>> b = a + 'There"
   >>> print(b)
   HelloThere
 

6. Dr. Chuck tells us that in can be used as a logical operator. Explain what this means and provide a few
   examples of its use this way with strings.

   You can use it to return a True or False value and you can use it to check if something is in a string.
   >>> fruit = 'banana'
   >>> 'n' in fruit
   True
   >>> 'z' in fruit
   False   


7. What happens when you use comparison operators (>, <, >=, <=, ==, !=) with strings?

   It compares them based on their lexicographic order and usually upper case is less than lower case. A good 
   amount of it depends on your computers keyboard settings.
   

8. What is a method? Which string methods does Dr. Chuck show us? Provide several examples.

   A method is is just a function that are built into every string and you can call them at the end of strings.
   Some methods are: capitalize, casefold, center, upper, lower, count, and find.
   >>> greet = 'Hello Bob'
   >>> zap = greet.lower()
   >>> print(zap)
   hello bob
   >>> zaps = greet.upper()
   >>> print(zaps)
   HELLO BOB

   >>> fruit = 'banana'
   >>> pos = fruit.find('na')
   >>> print(pos)
   2
  

9. Define white space.

   White space is non printing character like spaces and tabs. You can use strip() to remove white space at the
   beginning and end or you can us lstrip() or rstrip() to just remove one side of white space.


10. What is unicode?

    All strings are stored as unicode and it assigns characters and symbols a code point.
